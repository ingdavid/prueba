package com.daga.demo.repositor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.daga.demo.entity.Empleado;

@Repository
public interface Repo extends JpaRepository<Empleado, Long>{

	public abstract Empleado findByName (String name);
	public abstract Empleado findByNameAndId (String name, Long id);
	public abstract Empleado findByCity (String city);
	
	
}
