package com.daga.demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Empleado {

	@Id
	@GeneratedValue
	private long id;

	private String name;

	private String lastName;

	private String city;

	public Empleado(long id, String name, String lastName, String city) {

		this.id = id;
		this.name = name;
		this.lastName = lastName;
		this.city = city;
	}

	public Empleado() {

	}

	public Long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

}
