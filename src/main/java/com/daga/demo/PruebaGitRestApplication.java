package com.daga.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaGitRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaGitRestApplication.class, args);
	}
}
